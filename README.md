
# Ansible Role: Tower Setup

This role will install Ansible Tower on a specified virtual machine or instance. At the moment it does not specifically test for the minimum requirements (Base OS, memory, etc.) and will fail if they aren't.

## Tower Installation

### Prerequisites

Will isntall all dependencies needed for Tower to run.

### Tower

Perform the actual installation.

### Let's encrypt

Will deploy a Let's encrypt certificate to avoid browser warning messages. Make sure to point to the staging environment of Let's encrypt if you plan to redeploy often, or you will hit their rate limits.

Use the boolean "staging:true" (false by default) if you want this Role to use the staging servers.

## How to use

The role is optimized to be used from the [Playbook Tower](https://gitlab.com/redhat-cop/ansible-ssa/playbook-tower) which will be called by a Job Template created by this role.

If you want to use it in your playbook:

  - name: Install Tower
    include_role:
      name: tower_setup

Add the role to your requirements.yml or import it manually first.

## Variables

This role is using a list of variables to configure the Tower Server. Check the [defaults](defaults/main.yml) and adjust them to your needs.
